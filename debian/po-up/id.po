# Menu section translation to Bahasa Indonesia
# Copyright (C) 2003
# This file is distributed under the same license as the menu package.
# Bill Allombert <ballombe@debian.org>, 2003.
#
#
msgid ""
msgstr ""
"Project-Id-Version: menu-section 2.1.9-3\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-01-21 18:53-0500\n"
"PO-Revision-Date: 2007-07-19 17:19+0700\n"
"Last-Translator: Arief S Fitrianto <arief@gurame.fisika.ui.ac.id>\n"
"Language-Team: Debian Indonesian Team <debian-l10n-id@gurame.fisika.ui.ac."
"id>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: debian/desktop-files/ActionGames.directory.desktop.in:3
msgid "Action"
msgstr "Aksi"

#: debian/desktop-files/ActionGames.directory.desktop.in:4
msgid "Action games"
msgstr "Permainan aksi"

#. Translators: Do NOT translate or transliterate this text (this is an icon file name)!
#: debian/desktop-files/ActionGames.directory.desktop.in:6
msgid "weather-storm"
msgstr ""

#: debian/desktop-files/Settings-System.directory.desktop.in:4
msgid "Administration"
msgstr "Administrasi"

#: debian/desktop-files/Settings-System.directory.desktop.in:5
msgid "Change system-wide settings (affects all users)"
msgstr "Pengaturan sistem secara luas (mempengaruhi seluruh pengguna)"

#. Translators: Do NOT translate or transliterate this text (this is an icon file name)!
#: debian/desktop-files/Settings-System.directory.desktop.in:7
#, fuzzy
msgid "preferences-system"
msgstr "Preferensi"

#: debian/desktop-files/KidsGames.directory.desktop.in:3
msgid "Kids"
msgstr "Anak-anak"

#: debian/desktop-files/KidsGames.directory.desktop.in:4
msgid "Games for kids"
msgstr "Permainan anak-anak"

#. Translators: Do NOT translate or transliterate this text (this is an icon file name)!
#: debian/desktop-files/KidsGames.directory.desktop.in:6
msgid "gnome-amusements"
msgstr ""

#: debian/desktop-files/CardGames.directory.desktop.in:3
msgid "Cards"
msgstr "Kartu"

#: debian/desktop-files/CardGames.directory.desktop.in:4
msgid "Card games"
msgstr "Permainan kartu"

#. Translators: Do NOT translate or transliterate this text (this is an icon file name)!
#: debian/desktop-files/CardGames.directory.desktop.in:6
msgid "gnome-aisleriot"
msgstr ""

#: debian/desktop-files/Debian.directory.desktop.in:3
msgid "Debian"
msgstr "Debian"

#: debian/desktop-files/Debian.directory.desktop.in:4
msgid "The Debian menu"
msgstr "Menu Debian"

#. Translators: Do NOT translate or transliterate this text (this is an icon file name)!
#: debian/desktop-files/Debian.directory.desktop.in:6
msgid "debian-logo"
msgstr ""

#: debian/desktop-files/SimulationGames.directory.desktop.in:3
msgid "Simulation"
msgstr "Simulasi"

#: debian/desktop-files/SimulationGames.directory.desktop.in:4
msgid "Simulation games"
msgstr "Permainan simulasi"

#: debian/desktop-files/BoardGames.directory.desktop.in:3
msgid "Board"
msgstr "Di Atas Papan"

#: debian/desktop-files/BoardGames.directory.desktop.in:4
msgid "Board games"
msgstr "Permainan di atas papan"

#. Translators: Do NOT translate or transliterate this text (this is an icon file name)!
#: debian/desktop-files/BoardGames.directory.desktop.in:6
msgid "desktop"
msgstr ""

#: debian/desktop-files/GnomeScience.directory.desktop.in:3
msgid "Science"
msgstr "IPTEK"

#: debian/desktop-files/GnomeScience.directory.desktop.in:4
msgid "Scientific applications"
msgstr ""

#. Translators: Do NOT translate or transliterate this text (this is an icon file name)!
#: debian/desktop-files/GnomeScience.directory.desktop.in:6
msgid "applications-science"
msgstr ""

#: debian/desktop-files/StrategyGames.directory.desktop.in:3
msgid "Strategy"
msgstr "Taktik dan Strategi"

#: debian/desktop-files/StrategyGames.directory.desktop.in:4
msgid "Strategy games"
msgstr "Permainan taktik dan strategi"

#: debian/desktop-files/ArcadeGames.directory.desktop.in:3
msgid "Arcade"
msgstr "Pertempuran"

#: debian/desktop-files/ArcadeGames.directory.desktop.in:4
msgid "Arcade style games"
msgstr "Permainan bertema pertempuran"

#. Translators: Do NOT translate or transliterate this text (this is an icon file name)!
#: debian/desktop-files/ArcadeGames.directory.desktop.in:6
msgid "gnome-joystick"
msgstr ""

#: debian/desktop-files/Settings.directory.desktop.in:3
msgid "Preferences"
msgstr "Preferensi"

#: debian/desktop-files/Settings.directory.desktop.in:4
msgid "Personal preferences"
msgstr "Preferensi pribadi"

#. Translators: Do NOT translate or transliterate this text (this is an icon file name)!
#: debian/desktop-files/Settings.directory.desktop.in:6
#, fuzzy
msgid "preferences-desktop"
msgstr "Preferensi"

#: debian/desktop-files/BlocksGames.directory.desktop.in:3
msgid "Falling blocks"
msgstr "Tetris"

#: debian/desktop-files/BlocksGames.directory.desktop.in:4
msgid "Falling blocks games"
msgstr "Permainan tetris"

#. Translators: Do NOT translate or transliterate this text (this is an icon file name)!
#: debian/desktop-files/BlocksGames.directory.desktop.in:6
msgid "quadrapassel"
msgstr ""

#: debian/desktop-files/SportsGames.directory.desktop.in:3
msgid "Sports"
msgstr "Olahraga"

#: debian/desktop-files/SportsGames.directory.desktop.in:4
msgid "Sports games"
msgstr "Permainan olahraga"

#. Translators: Do NOT translate or transliterate this text (this is an icon file name)!
#: debian/desktop-files/SportsGames.directory.desktop.in:6
msgid "trophy-gold"
msgstr ""

#: debian/desktop-files/LogicGames.directory.desktop.in:3
msgid "Logic"
msgstr "Logika"

#: debian/desktop-files/LogicGames.directory.desktop.in:4
msgid "Logic and puzzle games"
msgstr "Permainan Logika dan teka-teki"

#. Translators: Do NOT translate or transliterate this text (this is an icon file name)!
#: debian/desktop-files/LogicGames.directory.desktop.in:6
msgid "system-run"
msgstr ""

#: debian/desktop-files/AdventureGames.directory.desktop.in:3
msgid "Adventure"
msgstr "Petualangan"

#: debian/desktop-files/AdventureGames.directory.desktop.in:4
msgid "Adventure style games"
msgstr "Permainan bertema petualangan"

#: debian/desktop-files/RolePlayingGames.directory.desktop.in:3
msgid "Role playing"
msgstr "Bermain kekuasaan"

#: debian/desktop-files/RolePlayingGames.directory.desktop.in:4
msgid "Role playing games"
msgstr "Permainan tentang kekuasaan"

#. Translators: Do NOT translate or transliterate this text (this is an icon file name)!
#: debian/desktop-files/RolePlayingGames.directory.desktop.in:6
msgid "avatar-default"
msgstr ""
