# translation of menu_po-sections_mk.po to Macedonian
# Menu section translation
# Copyright (C) 2003
# This file is distributed under the same license as the menu package.
#
# Bill Allombert <ballombe@debian.org>, 2003.
# Georgi Stanojevski <glisha@gmail.com>, 2006.
msgid ""
msgstr ""
"Project-Id-Version: menu_po-sections_mk\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-01-21 18:53-0500\n"
"PO-Revision-Date: 2006-10-20 11:03+0200\n"
"Last-Translator: Georgi Stanojevski <glisha@gmail.com>\n"
"Language-Team: Macedonian <ossm-members@hedona.on.net.mk>\n"
"Language: mk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.2\n"

#: debian/desktop-files/ActionGames.directory.desktop.in:3
#, fuzzy
msgid "Action"
msgstr "Образовни"

#: debian/desktop-files/ActionGames.directory.desktop.in:4
#, fuzzy
msgid "Action games"
msgstr "Образовни"

#. Translators: Do NOT translate or transliterate this text (this is an icon file name)!
#: debian/desktop-files/ActionGames.directory.desktop.in:6
msgid "weather-storm"
msgstr ""

#: debian/desktop-files/Settings-System.directory.desktop.in:4
msgid "Administration"
msgstr "Администрација"

#: debian/desktop-files/Settings-System.directory.desktop.in:5
msgid "Change system-wide settings (affects all users)"
msgstr ""
"Промени ги поставувањата на целиот систем (има ефект врз сите корисници)"

#. Translators: Do NOT translate or transliterate this text (this is an icon file name)!
#: debian/desktop-files/Settings-System.directory.desktop.in:7
#, fuzzy
msgid "preferences-system"
msgstr "Преференции"

#: debian/desktop-files/KidsGames.directory.desktop.in:3
msgid "Kids"
msgstr ""

#: debian/desktop-files/KidsGames.directory.desktop.in:4
msgid "Games for kids"
msgstr ""

#. Translators: Do NOT translate or transliterate this text (this is an icon file name)!
#: debian/desktop-files/KidsGames.directory.desktop.in:6
msgid "gnome-amusements"
msgstr ""

#: debian/desktop-files/CardGames.directory.desktop.in:3
#, fuzzy
msgid "Cards"
msgstr "Карти"

#: debian/desktop-files/CardGames.directory.desktop.in:4
msgid "Card games"
msgstr ""

#. Translators: Do NOT translate or transliterate this text (this is an icon file name)!
#: debian/desktop-files/CardGames.directory.desktop.in:6
msgid "gnome-aisleriot"
msgstr ""

#: debian/desktop-files/Debian.directory.desktop.in:3
msgid "Debian"
msgstr ""

#: debian/desktop-files/Debian.directory.desktop.in:4
msgid "The Debian menu"
msgstr ""

#. Translators: Do NOT translate or transliterate this text (this is an icon file name)!
#: debian/desktop-files/Debian.directory.desktop.in:6
msgid "debian-logo"
msgstr ""

#: debian/desktop-files/SimulationGames.directory.desktop.in:3
msgid "Simulation"
msgstr "Симулација"

#: debian/desktop-files/SimulationGames.directory.desktop.in:4
#, fuzzy
msgid "Simulation games"
msgstr "Симулација"

#: debian/desktop-files/BoardGames.directory.desktop.in:3
msgid "Board"
msgstr "На табла"

#: debian/desktop-files/BoardGames.directory.desktop.in:4
#, fuzzy
msgid "Board games"
msgstr "На табла"

#. Translators: Do NOT translate or transliterate this text (this is an icon file name)!
#: debian/desktop-files/BoardGames.directory.desktop.in:6
msgid "desktop"
msgstr ""

#: debian/desktop-files/GnomeScience.directory.desktop.in:3
msgid "Science"
msgstr "Наука"

#: debian/desktop-files/GnomeScience.directory.desktop.in:4
msgid "Scientific applications"
msgstr ""

#. Translators: Do NOT translate or transliterate this text (this is an icon file name)!
#: debian/desktop-files/GnomeScience.directory.desktop.in:6
msgid "applications-science"
msgstr ""

#: debian/desktop-files/StrategyGames.directory.desktop.in:3
msgid "Strategy"
msgstr "Стратегија"

#: debian/desktop-files/StrategyGames.directory.desktop.in:4
#, fuzzy
msgid "Strategy games"
msgstr "Стратегија"

#: debian/desktop-files/ArcadeGames.directory.desktop.in:3
msgid "Arcade"
msgstr "Аркадни"

#: debian/desktop-files/ArcadeGames.directory.desktop.in:4
msgid "Arcade style games"
msgstr ""

#. Translators: Do NOT translate or transliterate this text (this is an icon file name)!
#: debian/desktop-files/ArcadeGames.directory.desktop.in:6
msgid "gnome-joystick"
msgstr ""

#: debian/desktop-files/Settings.directory.desktop.in:3
msgid "Preferences"
msgstr "Преференции"

#: debian/desktop-files/Settings.directory.desktop.in:4
msgid "Personal preferences"
msgstr "Лични преференции"

#. Translators: Do NOT translate or transliterate this text (this is an icon file name)!
#: debian/desktop-files/Settings.directory.desktop.in:6
#, fuzzy
msgid "preferences-desktop"
msgstr "Преференции"

#: debian/desktop-files/BlocksGames.directory.desktop.in:3
msgid "Falling blocks"
msgstr ""

#: debian/desktop-files/BlocksGames.directory.desktop.in:4
msgid "Falling blocks games"
msgstr ""

#. Translators: Do NOT translate or transliterate this text (this is an icon file name)!
#: debian/desktop-files/BlocksGames.directory.desktop.in:6
msgid "quadrapassel"
msgstr ""

#: debian/desktop-files/SportsGames.directory.desktop.in:3
msgid "Sports"
msgstr "Спорт"

#: debian/desktop-files/SportsGames.directory.desktop.in:4
#, fuzzy
msgid "Sports games"
msgstr "Спорт"

#. Translators: Do NOT translate or transliterate this text (this is an icon file name)!
#: debian/desktop-files/SportsGames.directory.desktop.in:6
msgid "trophy-gold"
msgstr ""

#: debian/desktop-files/LogicGames.directory.desktop.in:3
msgid "Logic"
msgstr ""

#: debian/desktop-files/LogicGames.directory.desktop.in:4
msgid "Logic and puzzle games"
msgstr ""

#. Translators: Do NOT translate or transliterate this text (this is an icon file name)!
#: debian/desktop-files/LogicGames.directory.desktop.in:6
msgid "system-run"
msgstr ""

#: debian/desktop-files/AdventureGames.directory.desktop.in:3
msgid "Adventure"
msgstr "Авантура"

#: debian/desktop-files/AdventureGames.directory.desktop.in:4
#, fuzzy
msgid "Adventure style games"
msgstr "Авантура"

#: debian/desktop-files/RolePlayingGames.directory.desktop.in:3
msgid "Role playing"
msgstr ""

#: debian/desktop-files/RolePlayingGames.directory.desktop.in:4
msgid "Role playing games"
msgstr ""

#. Translators: Do NOT translate or transliterate this text (this is an icon file name)!
#: debian/desktop-files/RolePlayingGames.directory.desktop.in:6
msgid "avatar-default"
msgstr ""
